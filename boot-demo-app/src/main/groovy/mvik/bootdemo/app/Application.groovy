package mvik.bootdemo.app

import mvik.bootdemo.metrics.MetricsConfig
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Import

// tag::application[]
@SpringBootApplication
@Import([
    MetricsConfig
])
class Application {

    static void main(String[] args) {
        SpringApplication.run(Application, args)
    }
}
// end::application[]
