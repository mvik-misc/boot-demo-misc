package mvik.bootdemo.app.hello

import org.springframework.stereotype.Service

@Service
@SuppressWarnings("GrMethodMayBeStatic")
class HelloService {

    Map<String, String> createHelloMap(String name) {
        return [
            hello : name,
            length: name?.length()
        ] as Map
    }
}
