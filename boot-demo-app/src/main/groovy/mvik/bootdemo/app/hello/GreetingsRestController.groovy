package mvik.bootdemo.app.hello

import io.micrometer.core.instrument.Metrics
import io.micrometer.core.instrument.Timer
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

import java.util.function.Supplier

// tag::m-annotation-def[]
@RestController
@RequestMapping("/greetings")
class GreetingsRestController {

    private static final Timer TIMER = Metrics.timer('app.rest', 'tagName', 'tagValue')
    // end::m-annotation-def[]

    private final HelloService helloService
    private final GoodbyeService goodbyeService

    GreetingsRestController(HelloService helloService, GoodbyeService goodbyeService) {
        this.helloService = helloService
        this.goodbyeService = goodbyeService
    }

    // tag::m-annotation-method[]
    @GetMapping(path = "/hello")
    Map hello(@RequestParam('name') String name) {
        return TIMER.record(
            { -> helloService.createHelloMap(name) } as Supplier<Map>
        )
    }
    // end::m-annotation-method[]

    @GetMapping(path = "/goodbye")
    Map goodbye(@RequestParam('name') String name) {
        return goodbyeService.sayGoodbye(name)
    }
}
