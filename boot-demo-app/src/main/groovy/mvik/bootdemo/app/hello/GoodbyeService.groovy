package mvik.bootdemo.app.hello

import io.micrometer.core.annotation.Timed
import org.springframework.stereotype.Service

import java.util.concurrent.ThreadLocalRandom

@Service
@SuppressWarnings("GrMethodMayBeStatic")
class GoodbyeService {

    // tag::m-annotation-method[]
    @Timed(value = 'app.greetings', extraTags = ['type', 'goodbye'])
    Map<String, String> sayGoodbye(String name) {
        def waitMillis = ThreadLocalRandom.current().nextInt(500, 3000)
        Thread.sleep(waitMillis)
        return [goodbye: name, waited : waitMillis] as Map
    }
    // end::m-annotation-method[]
}
