package mvik.bootdemo.app.config

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import groovy.sql.Sql
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.testcontainers.containers.PostgreSQLContainer

import javax.sql.DataSource

@Configuration
class DatabaseConfig {

    @Bean Sql sql(DataSource ds) {
        new Sql(ds)
    }

    @Bean(initMethod = 'start', destroyMethod = 'stop')
    PostgreSQLContainer demodb() {
        return new PostgreSQLContainer('postgres:10.4-alpine')
            .withDatabaseName('bootdemo')
            .withUsername('bd_rw')
            .withPassword('bd_rw_pass')
            .tap { it.withExposedPorts(5432) }
    }

    @Bean DataSource dataSource(PostgreSQLContainer demodb) {
        new HikariDataSource(
            new HikariConfig(
                jdbcUrl: demodb.jdbcUrl,
                username: demodb.username,
                password: demodb.password,
                driverClassName: 'org.postgresql.Driver'
            )
        )
    }

}
