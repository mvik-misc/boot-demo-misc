package mvik.bootdemo.spec.simple

import cucumber.api.CucumberOptions
import cucumber.api.SnippetType
import cucumber.api.junit.Cucumber
import org.junit.runner.RunWith

// tag::partial[]
@RunWith(Cucumber)
@CucumberOptions(
    features = ['src/main/features/simple'],
    glue = ['mvik.bootdemo.spec.shared', 'mvik.bootdemo.spec.simple'],
    plugin = ['pretty'],
    tags = ['not @wip'],
    strict = true,
    snippets = SnippetType.CAMELCASE
)
class SimpleFeatures {
}
// end::partial[]