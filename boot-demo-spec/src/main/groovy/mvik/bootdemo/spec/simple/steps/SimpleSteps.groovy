package mvik.bootdemo.spec.simple.steps

import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class SimpleSteps {

    private int sum

    @When('adding {int} + {int}')
    def add(int a, int b) {
        sum = a + b
    }

    @Then('the result is {int}')
    def assertSum(int expected) {
        assert sum == expected
    }
}
