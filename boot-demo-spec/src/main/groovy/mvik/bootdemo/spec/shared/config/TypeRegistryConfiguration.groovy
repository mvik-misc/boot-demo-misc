package mvik.bootdemo.spec.shared.config

import cucumber.api.TypeRegistry
import cucumber.api.TypeRegistryConfigurer
import io.cucumber.cucumberexpressions.ParameterType
import io.cucumber.cucumberexpressions.Transformer

import java.time.LocalDate

@SuppressWarnings("unused")
class TypeRegistryConfiguration implements TypeRegistryConfigurer {

    final static String REGEX_DATE = ~'\\d{4}-\\d{2}-\\d{2}'

    @Override
    Locale locale() {
        return Locale.ENGLISH
    }

    @Override
    void configureTypeRegistry(TypeRegistry typeRegistry) {

        typeRegistry.defineParameterType(new ParameterType<LocalDate>(
            'date',
            "'${REGEX_DATE}'",
            LocalDate,
            { String arg -> LocalDate.parse(arg.replace('\'', '')) } as Transformer<LocalDate>
        ))

    }
}
