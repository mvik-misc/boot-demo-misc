package mvik.bootdemo.spec.app

import cucumber.api.CucumberOptions
import cucumber.api.SnippetType
import cucumber.api.junit.Cucumber
import org.junit.runner.RunWith

// tag::partial[]
@RunWith(Cucumber)
@CucumberOptions(
    features = ['src/main/features/api'],
    glue = ['mvik.bootdemo.spec.shared', 'mvik.bootdemo.spec.app'],
    plugin = ['pretty'],
    tags = ['not @wip'],
    strict = true,
    snippets = SnippetType.CAMELCASE
)
class AppFeatures {
}
// end::partial[]