package mvik.bootdemo.spec.app.config

import mvik.bootdemo.app.Application
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

// tag::cuke-config[]
@Configuration
@ComponentScan(basePackageClasses = Application)
class CukeConfig {
}
// end::cuke-config[]