package mvik.bootdemo.spec.app.steps

import com.fasterxml.jackson.databind.ObjectMapper
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovy.sql.Sql
import groovy.transform.CompileStatic
import io.cucumber.datatable.DataTable
import mvik.bootdemo.spec.app.config.CukeConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.context.ContextConfiguration

import java.time.LocalDate

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@CompileStatic
@SuppressWarnings(["GroovyUnusedAssignment", "GrMethodMayBeStatic"])
// tag::step-config[]
@ContextConfiguration(classes = [CukeConfig])
@SpringBootTest(webEnvironment = RANDOM_PORT)
class DemoSteps {

    @LocalServerPort int port
    @Autowired ObjectMapper objectMapper
// end::step-config[]
    @Autowired Sql sql

    @Given("a fresh environment")
    def aFreshEnvironment() {
        println "We are fresh. Go!"
    }

// tag::step-methods[]
    private Map mappedResponse

    @When('GET {string}')
    def methodGET(String uriPath) {
        def response = "http://localhost:${port}/api/${uriPath}"
            .toURL()
            .newReader([requestProperties: [Authorization: "Basic ${'user:pass'.bytes.encodeBase64()}"]])
            .text

        mappedResponse = objectMapper.readValue(response, Map)

    }

    @Then('the response matches:')
    def assertGET(DataTable data) {
        Map<String, String> asserts = data.asMap(String, String)

        asserts.each { k, v ->
            assert mappedResponse[k]?.toString() == v
        }
    }
// end::step-methods[]

    @Then('print {date}')
    def printDate(LocalDate date) {
        println "date: ${date} <- ${date.getClass()}"
    }

    @Given('show all from tables:')
    def showAllFromTable(DataTable data) {
        data.asList().each { String tableName ->
            println "All from ${tableName}:"
            sql.eachRow("select * from ${tableName}" as String) {
                println it
            }
        }
    }
}
