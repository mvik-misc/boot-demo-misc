# tag::cuke-example[]
@api
Feature: rest call

  Background:
    Given a fresh environment

  @valid
  Scenario: say hello to someone
    When GET '/greetings/hello?name=Bjarne'
    Then the response matches:
      | hello  | Bjarne |
      | length | 6      |
# end::cuke-example[]

  @valid
  Scenario: demo stuff...
    When print '2019-01-23'
    Then show all from tables:
      | user_account |
