@simple @add
Feature: no spring stuff, fast stuff..

  Scenario: sum stuff up
    When adding 3 + 4
    Then the result is 7

  @wip
  Scenario: sum stuff up, fails when run
    When adding 13 + 4
    Then the result is 10

  @unroll
  Scenario Outline: same as Spock @Unroll
    When adding <a> + <b>
    Then the result is <expected>
    Examples:
      | a  | b   | expected |
      | 1  | 1   | 2        |
      | 2  | 2   | 4        |
      | 13 | 13  | 26       |
      | 27 | -13 | 14       |
