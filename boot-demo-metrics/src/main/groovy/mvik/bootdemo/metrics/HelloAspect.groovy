package mvik.bootdemo.metrics

import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Tags
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect

@SuppressWarnings("unused")
// tag::m-aspect-h[]
@Aspect
class HelloAspect {

    private final MeterRegistry registry
// end::m-aspect-h[]

    HelloAspect(MeterRegistry registry) {
        this.registry = registry
    }

// tag::m-aspect-m[]
    @Around('execution(* mvik.bootdemo.app.hello.HelloService.*(..))')
    Object timed(ProceedingJoinPoint pjp) throws Throwable {
        return MetricsMethods.timeMethodInvocation(
            registry,
            pjp,
            'app.greetings',
            Tags.of(
                "type", 'hello',
                'length', extractArg0Length(pjp.getArgs())
            )
        )
    }
// end::m-aspect-m[]

    private static String extractArg0Length(Object[] args) {
        if (args == null || args.length == 0) {
            return -2
        }
        return args[0]?.toString()?.length()
    }
}
