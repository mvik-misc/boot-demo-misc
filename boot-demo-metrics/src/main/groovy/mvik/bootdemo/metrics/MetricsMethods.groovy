package mvik.bootdemo.metrics

import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Tag
import io.micrometer.core.instrument.Tags
import io.micrometer.core.instrument.Timer
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.Signature

class MetricsMethods {

    static Object timeMethodInvocation(MeterRegistry registry,
                                       ProceedingJoinPoint pjp,
                                       String name,
                                       Iterable<Tag> extraTags) throws Throwable {

        Timer.Sample sample = Timer.start(registry)
        try {
            return pjp.proceed()
        } finally {
            sample.stop(Timer.builder(name)
                .tags(extraTags)
                .tags(tagsBasedOnJoinpoint(pjp))
                .register(registry))
        }
    }

    private static Iterable<Tag> tagsBasedOnJoinpoint(ProceedingJoinPoint pjp) {
        Signature signature = pjp.getStaticPart().getSignature()
        return Tags.of(
            "class", signature.getDeclaringTypeName(),
            "method", signature.getName()
        )
    }
}
