package mvik.bootdemo.metrics

import io.micrometer.core.aop.TimedAspect
import io.micrometer.core.instrument.MeterRegistry
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.EnableAspectJAutoProxy
import org.springframework.context.annotation.Import

@Configuration
@EnableAspectJAutoProxy
@Import([
    HelloAspect
])
class MetricsConfig {

    /**
     * Enables the collection of metrics from methods annotated with @Timed.
     *
     * Still necessary in Spring Boot 2.1.
     */
    @Bean
    TimedAspect timedAspect(MeterRegistry registry) {
        return new TimedAspect(registry)
    }
}
